package com.tugasqc.ahsaniafif;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AhsaniafifApplication {

	public static void main(String[] args) {
		SpringApplication.run(AhsaniafifApplication.class, args);
	}

}
